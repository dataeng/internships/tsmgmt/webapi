# webapi

WebAPI for executing analytics on the signals stored in the database

Access the WebAPI by: http://18.185.240.130:31000/

The REST API is developed using Python Django and its REST framework. With this repository, Django is deployed as a k8s pod and we expose it as a NodePort service, in order to access it.

If you're not familiar with Django's architecture, you may read the Django-Specifics.txt file. 

After deploying Django and exposing its service, we use its REST framework in order to perform CRUD operations on our data. Some technicalities about REST are also mentioned in the Django-Specifics.txt file. 

Next up, here are the endpoints currently supported by the REST API. You can also view the endpoints at the root URL of the Django service. (Reminder: the IPs you need are the ones from the k8s cluster nodes. Port 31000 is the one forwarded by NodePort.)

One more note: POST endpoints are available through wget or CURL. You may use the browser as well, however this is not an intended usage. DELETE endpoints are available only by CURL. 

`demoapp/RandomData`

method: POST

arguments:

    - iot1_device_name (String value)
    - iot2_device_name (String value)
    - analytics_device_name (String value)

This method creates Random Data using the simulated iot1, iot2 and analytics devices. You may use one, two or all three arguments (but not none).

Sample queries:

wget http://18.185.240.130:31000/demoapp/RandomData --post-data 'iot1_device_name=iot1random&iot2_device_name=iot2random&analytics_device_name=analyticsrandom'

method: GET

arguments:

    - table (May receive values of 'temp_01', 'ph_01' and 'analytics_01'. If this argument is empty or receives a different value, an error message will be returned)
    - sequence (Set either 'true' or 'false'. May also be empty)

This method gets Random Data from the DB. Sequence argument returns the data in an array with all the values so the user can easily pick the ones he wants for posting/getting similar spans (see below).

Sample queries:

http://18.185.240.130:31000/demoapp/RandomData?table=temp_01&sequence=true

method: DELETE

arguments:

    - iot1_device_name (String value)
    - iot2_device_name (String value)
    - analytics_device_name (String value)

Deleting the pods that send Random data to the DB. You may use one, two or all three arguments (but not none).

Sample queries:

curl -X DELETE http://18.185.240.130:31000/demoapp/RandomData -H "Content-Type:application/json" -d '{"iot1_device_name":"iot1random", "iot2_device_name":"iot2random", "analytics_device_name":"analyticsrandom"}'


`demoapp/AdlData`

method: POST

arguments:

    - label_device_name (String value)
    - label_volunteer (May receive values between m1-11 and f1-5)
    - acc_device_name (String value)
    - acc_volunteer (May receive values between m1-11 and f1-5)

Creating ADL data using iot1 and iot2 devices. You may start one of two devices if you want but make sure to provide both arguments for one device (i.e. for the accelerometer device you must set both the device_name and the volunteer)

Sample queries:

wget http://18.185.240.130:31000/demoapp/AdlData --post-data 'acc_device_name=iot1m7&acc_volunteer=m7&label_device_name=iot2m7&label_volunteer=m7'

method: GET

arguments:

    - table (May receive values of 'adl_acc_01' and 'adl_labels_01'. If this argument is empty or receives a different value, an error message will be returned)
    - activity (May receive values of the ADL activities. See the full list of available values at the end of the page)
    - volunteer (May receive values between m1-11 and f1-5)

Gets data from the ADL tables (acc data or labels). Activity argument is not available for the adl_acc_01 table. 

Sample queries:

http://18.185.240.130:31000/demoapp/AdlData?table=adl_labels_01&volunteer=m7

method: DELETE

arguments:

    - label_device_name (String value)
    - acc_device_name (String value)

Deleting the pods that were used for the ADL data creation. You may use one or two arguments (but not none).

Sample queries:

curl -X DELETE http://18.185.240.130:31000/demoapp/AdlData -H "Content-Type:application/json" -d '{"acc_device_name":"iot1m7", "label_device_name":"iot2m7"}'

`demoapp/SimilarSpans`

method: POST

arguments:

    - query (Array value)
    - table (May receive values of 'temp_01', 'ph_01' and 'analytics_01'. If this argument is empty or receives a different value, an error message will be returned)
    - slice_size (Integer value. If empty, it will default to 3)
    - threshold (Float value. If empty, it will default to 1.2)

The query is made up by an array containing values from one of the three table. Keep in mind that the query must exist in the database otherwise an error message will be returned. In order to easily pick values from the tables, it is advised to use the GET method of RandomData and the sequence argument. The threshold argument is used to check how many items will be returned. It will return all items that have a smaller or equal difference stored in the database.

Sample queries:

wget http://18.185.240.130:31000/demoapp/SimilarSpans --post-data 'query=[4.06, 3.42, 2.94, 6.01, 3.88, 2.18, 2.43, 2.78, 7.54, 6.38]&table=temp_01&slice_size=3&threshold=1.2'

method: GET

arguments:

    - query (Array value)
    - table (May receive values of 'temp_01', 'ph_01' and 'analytics_01'. If this argument is empty or receives a different value, an error message will be returned)
    - difference (Float value. If empty, it will default to 1.2)


Arguments are the same ones as for the POST method. Important note: Make sure the query signal is already existent in the database, otherwise an empty array will be returned by the API.

Sample queries:

http://18.185.240.130:31000/demoapp/SimilarSpans?query=[4.06, 3.42, 2.94, 6.01, 3.88, 2.18, 2.43, 2.78, 7.54, 6.38]&table=temp_01&difference=1.2


`demoapp/AdlClassify`

method: POST

arguments:

    - volunteer (May receive values between m1-11 and f1-5)

Creates a pod (named iot1+<volunteer_arg>) that begins the classification of a volunteer signal using the iot1 device.

Sample queries:

wget http://18.185.240.130:31000/demoapp/AdlClassify --post-data 'volunteer=m1'

method: GET

arguments:

    - table (May receive values of 'adl_classify_1', 'adl_classify_matches_2' and 'adl_classify_labels_2'. If this argument is empty or receives a different value, an error message will be returned)
    - volunteer (May receive values between m1-11 and f1-5)
    - segment_query (String value. Made up of the word 'Segment' and a number)
    - volunteer_match (May receive values between m1-11 and f1-5)
    - segment_match (String value. Made up of the word 'Segment' and a number)
    - label_match (May receive values of the ADL activities. See the full list of available values at the end of the page)
    - label_true (May receive values of the ADL activities. See the full list of available values at the end of the page)
    - difference (Float value)


Returns data from the classification-related tables. adl_classify_1 table may receive the  volunteer and segment_query arguments only. The other arguments can be used to get the matches from the other tables. Important note: Since the accelerometer data are a bit large, it is advised to use the adl_classify_labels_2 table to retrieve data, since it doesn't have any acc data.

Sample queries:

http://18.185.240.130:31000/demoapp/AdlClassify?table=adl_classify_labels_2&volunteer=m1

method: DELETE

arguments:

    - device_name (String value)

Deleting the pod used for ADL-classify creation. Remember that its name is the word 'iot1' and the volunteer id you used.


Sample queries:

curl -X DELETE http://18.185.240.130:31000/demoapp/AdlClassify -H "Content-Type:application/json" -d '{"device_name":"iot1m1"}'


`demoapp/PopulateDB`

method: POST

arguments:

    - volunteer (May receive values between m1-11 and f1-5)

This endpoint can  be used to populate the database so the user may easily start the classification of a volunteer. The argument may seem misleading, so remember: This method creates an iot1 and iot2 device for every single volunteer except the one given by the argument. The idea is that we want to classify the signal of the given volunteer, so we fill the database with the signals of everyone else.

Sample queries:

wget http://18.185.240.130:31000/demoapp/PopulateDB --post-data 'volunteer=m1'

`demoapp/DepopulateDB`

method: DELETE

arguments:

    - volunteer (May receive values between m1-11 and f1-5)

Deletes all the pods used to populate the database. Keep note of the argument from the description of the previous endpoint.

Sample queries:

curl -X DELETE http://18.185.240.130:31000/demoapp/DepopulateDB -H "Content-Type:application/json" -d '{"volunteer":"m1"}'

`demoapp/DeleteDevice`

method: DELETE 

arguments:

    - device_name (String value)

Various pods may be created from the previous endpoints so you may use this one to delete one a bit faster.

Sample queries:

curl -X DELETE http://18.185.240.130:31000/demoapp/DeleteDevice -H "Content-Type:application/json" -d '{"device_name":"iot1m1"}'

ADL activities available:
- brush_teeth
- climb_stairs
- comb_hair
- descend_stairs
- drink_glass
- eat_meat
- eat_soup
- getup_bed
- liedown_bed
- pour_water
- sitdown_chair
- standup_chair
- use_telephone
- walk


You may continue to the Grafana service at: http://18.185.240.130:31861/ (Also the README.md is available at: https://gitlab.com/dataeng/internships/tsmgmt/vui/-/blob/main/README.md)

