from django.shortcuts import render
from .serializers import *
from django.core import serializers
from demoapp.models import *
from django.apps import apps

from rest_framework import viewsets
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAdminUser, IsAuthenticated
from rest_framework.response import Response

from dtw import *
from datetime import datetime, timezone
import ast, os, re, subprocess

# Create your views here.

#Demo view used for testing.
def detail(request):

    data = Analytics01.objects.all()
    context= {'all': data}
    return render(request, 'home.html', context)


#Note: Permissions need to change in the future

#The ViewSet classes can be used in order to perform CRUD operations on the models we have. Currently, these ViewSets cannot be used (there is no endpoint to support them), but I decided to keep them in case they appear to be useful in the future.
class AnalyticsViewSet(viewsets.ModelViewSet):
    queryset = Analytics01.objects.all()
    serializer_class = AnalyticsSerializer

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        permission_classes = [AllowAny]
        return [permission() for permission in permission_classes]


class PhViewSet(viewsets.ModelViewSet):
    queryset = Ph01.objects.all()
    serializer_class = PhSerializer

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        permission_classes = [AllowAny]
        return [permission() for permission in permission_classes]


class TemperatureViewSet(viewsets.ModelViewSet):
    queryset = Temp01.objects.all()
    serializer_class = TemperatureSerializer

    def get_permissions(self):
        """
        Instantiates and returns the list of permissions that this view requires.
        """
        permission_classes = [AllowAny]
        return [permission() for permission in permission_classes]


#Class for the Random Data endpoint.
class RandomData(APIView):

    permission_classes = [AllowAny]

    #Creating random data using iot-1, iot-2 and analytics devices.
    def post(self, request, format=None):

        parameters = request.data.keys()
        
        iot1_set = True
        iot1 = ''
        if 'iot1_device_name' not in parameters:
            iot1_set = False
        else:
            iot1 = request.data['iot1_device_name']

        iot2_set = True
        iot2 = ''
        if 'iot2_device_name' not in parameters:
            iot2_set = False
        else:
            iot2 = request.data['iot2_device_name']

        analytics_set = True
        analytics = ''
        if 'analytics_device_name' not in parameters:
            analytics_set = False
        else:
            analytics = request.data['analytics_device_name']

        if iot1_set == False and iot2_set == False and analytics_set == False:
            return Response('Must set at least one device.')
        
        if analytics_set == True:
            command = 'kubectl run ' + analytics +  ' --image=registry.gitlab.com/dataeng/internships/tsmgmt/analytics:v2 --overrides=\'{ "apiVersion": "v1", "spec": {"imagePullSecrets": [{"name": "regcred"}]} } \' '
            os.system(command)

        if iot2_set == True:
            command = 'kubectl run ' + iot2 + ' --image=registry.gitlab.com/dataeng/internships/tsmgmt/iot-device-2:v4 --overrides=\'{ "apiVersion": "v1", "spec": {"imagePullSecrets": [{"name": "regcred"}]} }\' --command -- python3 /opt/vb/iot2.py random'
            os.system(command)

        if iot1_set == True:
            command = 'kubectl run ' + iot1 + ' --image=registry.gitlab.com/dataeng/internships/tsmgmt/iot-device-1:v5 --overrides=\'{ "apiVersion": "v1", "spec": {"imagePullSecrets": [{"name": "regcred"}]} }\' --command -- python3 /opt/vb/iot1.py random'
            os.system(command)

        return Response('All required devices have started running successfully.')

    #Getting Random-based data from the DB.
    def get(self, request, format=None):
        
        table = ''
        if 'table' not in request.GET or request.GET['table'] not in iot_models:
            return Response('Table argument is not set.')
        else:
            table = request.GET['table']

        model = apps.get_model('demoapp',iot_models[table])
        models = model.objects.all().order_by('time')
        
        seq_mode = False
        if 'sequence' in request.GET:
            if request.GET['sequence'] == 'true':
                seq_mode = True

        #Returning data from the DB.
        if seq_mode == False:
            dtw_serializer_class = serializers_dict[model.__name__]
            dtw_serializer = dtw_serializer_class(models, many=True)
            return Response(dtw_serializer.data)
        #Sequence mode returns an array with all the values so the user can easily pick the ones he wants for posting/getting similar spans.
        else:
            attribute = attr_dict[table]
            sequence_array = [float(getattr(r, attribute)) for r in models]
            sequence_array = str(sequence_array)
            item = SequenceModel(field=attribute, query=sequence_array)
            ser = SequenceSerializer(item)
            return Response(ser.data)
        

        return Response('Normally you shouldn\'t be able to get here.')   

    #Deleting the pods that send Random data to the DB.
    def delete(self, request, format=None):

        parameters = request.data.keys()
        
        iot1_set = True
        iot1 = ''
        if 'iot1_device_name' not in parameters:
            iot1_set = False
        else:
            iot1 = request.data['iot1_device_name']

        iot2_set = True
        iot2 = ''
        if 'iot2_device_name' not in parameters:
            iot2_set = False
        else:
            iot2 = request.data['iot2_device_name']

        analytics_set = True
        analytics = ''
        if 'analytics_device_name' not in parameters:
            analytics_set = False
        else:
            analytics = request.data['analytics_device_name']

        if iot1_set == False and iot2_set == False and analytics_set == False:
            return Response('Must set at least one device.')
        
        if analytics_set == True:
            #os.system(command)
            command = 'kubectl delete pod ' + analytics +  ' '
            try:
                subprocess.check_output([command],  shell=True, stderr=subprocess.STDOUT)
            except subprocess.CalledProcessError as e:
                return Response('The analytics pod you provided does not exist.')

        if iot2_set == True:
            command = 'kubectl delete pod ' + iot2 +  ' '
            try:
                subprocess.check_output([command],  shell=True, stderr=subprocess.STDOUT)
            except subprocess.CalledProcessError as e:
                return Response('The iot2 pod you provided does not exist.')

        if iot1_set == True:
            command = 'kubectl delete pod ' + iot1 +  ' '
            try:
                subprocess.check_output([command],  shell=True, stderr=subprocess.STDOUT)
            except subprocess.CalledProcessError as e:
                return Response('The iot1 pod you provided does not exist.')

        return Response('All required devices have been deleted successfully.')


#Class used for the ADL data endpoint.
class AdlData(APIView):

    permission_classes = [AllowAny]

    #Creating ADL data using iot1 and iot2 devices.
    def post(self, request, format=None):

        parameters = request.data.keys()

        label_dev_set = True
        label_dev = ''
        if 'label_device_name' not in parameters:
            label_dev_set = False
        else:
            label_dev = request.data['label_device_name']

        label_vol_set = True
        label_vol = ''
        if 'label_volunteer' not in parameters:
            label_vol_set = False
        else:
            label_vol = request.data['label_volunteer']

        if label_dev_set == True and label_vol_set == False:
            return Response('Must set a volunteer for the labels device.')
        
        if label_vol_set == True and label_dev_set == False:
            return Response('Must set a device name for the volunteer.')

        if label_dev_set and label_vol_set:
            command = 'kubectl run ' + label_dev + ' --image=registry.gitlab.com/dataeng/internships/tsmgmt/iot-device-2:v4 --overrides=\'{ "apiVersion": "v1", "spec": {"imagePullSecrets": [{"name": "regcred"}]} }\' --command -- python3 /opt/vb/iot2.py adl ' + label_vol + ' '
            os.system(command)
        

        acc_dev_set = True
        acc_dev = ''
        if 'acc_device_name' not in parameters:
            acc_dev_set = False
        else:
            acc_dev = request.data['acc_device_name']

        acc_vol_set = True
        acc_vol = ''
        if 'acc_volunteer' not in parameters:
            acc_vol_set = False
        else:
            acc_vol = request.data['acc_volunteer']

        if acc_dev_set == True and acc_vol_set == False:
            return Response('Must set a volunteer for the accelerometer device.')
        
        if acc_vol_set == True and acc_dev_set == False:
            return Response('Must set a device name for the volunteer.')

        if acc_dev_set and acc_vol_set:
            command = 'kubectl run ' + acc_dev + ' --image=registry.gitlab.com/dataeng/internships/tsmgmt/iot-device-1:v5 --overrides=\'{ "apiVersion": "v1", "spec": {"imagePullSecrets": [{"name": "regcred"}]} }\' --command -- python3 /opt/vb/iot1.py adl ' + acc_vol + ' '
            os.system(command)


        return Response('All required devices have started running successfully.')

    #Getting data from the DB.
    def get(self, request, format=None):
        
        adl_tables = ['adl_acc_01','adl_labels_01']

        table = ''
        if 'table' not in request.GET or request.GET['table'] not in adl_tables:
            return Response('Table argument is not set.')
        else:
            table = request.GET['table']

        activity_arg = ''
        activity_set = True
        if 'activity' not in request.GET:
            activity_set = False
        elif 'activity' in request.GET and request.GET['activity'] not in activity_table:
            return Response('Choose an activity from the activity table. Use the home page for reference.')
        else:
            activity_arg = request.GET['activity']
        
        if table == 'adl_acc_01' and activity_set:
            return Response('Activities are not available on the accelerometer table.')

        range_1 = range(1, 12)
        list_1 = list(range_1)
        str_list1 = []
        for i in list_1:
            str_list1.append('m'+str(i))

        range_2 = range(1, 6)
        list_2 = list(range_2)
        str_list2 = []
        for i in list_2:
            str_list2.append('f'+str(i))

        vols = str_list1+str_list2

        volunteer_arg = ''
        volunteer_set = True
        if 'volunteer' not in request.GET:
            volunteer_set = False
        elif 'volunteer' in request.GET and request.GET['volunteer'] not in vols:
            return Response('Choose a volunteer between m1-11 or f1-5.')
        else:
            volunteer_arg = request.GET['volunteer']

        if table == 'adl_labels_01':
            label_data = AdlLabels01.objects.all()
            if activity_set:
                label_data = label_data.filter(activity=activity_arg)
            if volunteer_set:
                label_data = label_data.filter(volunteer=volunteer_arg)
            if activity_set and volunteer_set:
                label_data = label_data.filter(activity=activity_arg, volunteer=volunteer_arg)
            
            adlSerializer = AdlLabelsSerializer(label_data, many=True)
            return Response(adlSerializer.data)

        if table == 'adl_acc_01':
            acc_data = AdlAcc01.objects.all()
            if volunteer_set:
                acc_data = acc_data.filter(volunteer=volunteer_arg)
            
            
            adlSerializer = AdlAccSerializer(acc_data, many=True)
            return Response(adlSerializer.data)

        return Response('Normally you shouldn\'t be able to get here.')

    #Deleting the pods that were used for the ADL data creation.
    def delete(self, request, format=None):

        parameters = request.data.keys()

        label_dev_set = True
        label_dev = ''
        if 'label_device_name' not in parameters:
            label_dev_set = False
        else:
            label_dev = request.data['label_device_name']

        
        acc_dev_set = True
        acc_dev = ''
        if 'acc_device_name' not in parameters:
            acc_dev_set = False
        else:
            acc_dev = request.data['acc_device_name']


        if acc_dev_set == False and label_dev_set == False:
            return Response('Must set at least one device.')

        if label_dev_set:
            command = 'kubectl delete pod ' + label_dev + ' '
            try:
                subprocess.check_output([command],  shell=True, stderr=subprocess.STDOUT)
            except subprocess.CalledProcessError as e:
                return Response('The labels pod you provided does not exist.')

#            os.system(command)        
        

        if acc_dev_set:
            command = 'kubectl delete pod ' + acc_dev + ' '
            try:
                subprocess.check_output([command],  shell=True, stderr=subprocess.STDOUT)
            except subprocess.CalledProcessError as e:
                return Response('The accelerometer pod you provided does not exist.')


        return Response('All required devices have been deleted successfully.')


#Helper functions used for changing the Segment columns of the Similar Spans table (see below.)
def atoi(text):
    return int(text) if text.isdigit() else text

def natural_keys(text):
    return [ atoi(c) for c in re.split(r'(\d+)', text) ]


#This view is used to post similar spans found using the DTW method on the DB.
class SimilarSpans(APIView):

    permission_classes = [AllowAny]

    #Posting SimilarSpans data on the DB. K8s is not used here.
    def post(self, request, format=None):

        parameters = request.data.keys()

        query_arg = ''
        if 'query' not in parameters:
            return Response('Query argument is not set.')
        else:
            query_arg = request.data['query']

        query_arg = ast.literal_eval(query_arg)

        table = ''
        if 'table' not in parameters or request.data['table'] not in iot_models:
            return Response('Table argument is not set.')
        else:
            table = request.data['table']
        

        slice_size = 0
        if 'slice_size' not in parameters:
            slice_size = 3
        else:
            slice_size = request.data['slice_size']
            if slice_size.isnumeric():
                slice_size = int(slice_size)
            else:
                slice_size = 3

        
        threshold_arg = 0.0
        if 'threshold' not in parameters:
            threshold_arg = 1.2
        else:
            threshold_arg = request.data['threshold']
            
            is_float = False

            try:
                float(threshold_arg)
                is_float = True
            except ValueError:
                is_float = False

            if is_float:
                threshold_arg = float(threshold_arg)
            else:
                threshold_arg = 1.2

        model = apps.get_model('demoapp',iot_models[table])
        attribute = attr_dict[table]
        results_all = model.objects.all().order_by('time')
        timed_data = [(float(getattr(r, attribute)),getattr(r,'time')) for r in results_all]

        if len(query_arg) > len(results_all):
            return Response('Query\'s size exceeds table limits.')
        if slice_size > len(results_all):
            return Response('Slice size exceeds table limits.')


        results_atr = [float(getattr(r, attribute)) for r in results_all]
        #exists = any(query_arg == results_atr[i:i+(len(query_arg))] for i in range(len(results_atr) - 1))

        #Finding the requested signal in the DB and getting its timestamps.
        index_start = 0
        index_end = 0
        exists = False
        for i in range(len(results_atr) - 1):
            if query_arg == results_atr[i:i+(len(query_arg))]:
                exists = True
                index_start = i
                index_end = i+(len(query_arg)) 
                break

        times = timed_data[index_start:index_end]
    
        #print(array_all[7:10])

        if exists != True:
            return Response('Query is not in the table.')

        
        dtw_all = DtwQuery.objects.all()
        dtw_all2 = dtw_all.filter(field=attribute)
        segs = [getattr(r,'segment_query') for r in dtw_all2]
        
        #Changing the Segment column.
        segment_str = ''
        if segs != []:
            segs.sort(key=natural_keys)
            last_seg = segs[-1]
            ide = last_seg[7:]
            new_ide = int(ide) + 1
            segment_str = 'Segment'+str(new_ide)
        else:
            segment_str = 'Segment1'

        #Posting the requested query in the DB.
        for q in times:
            dtw_object = DtwQuery(field = attribute, value = q[0], segment_query = segment_str, time=q[1])
            dtw_object.save()
        

        slices = [results_atr[i:i + slice_size] for i in range(0, len(results_atr), slice_size)]
        if len(slices[-1]) < slice_size: #Remove the last slice if it doesn't have the right size.
            slices = slices[:-1]
        
        #Getting DTW results.
        similar_vals = []
        for slc in slices:
            ds = dtw(query_arg,slc,keep_internals=True)
            diff = round(ds.normalizedDistance,2)
            if diff < threshold_arg:
                similar_vals.append((slc,diff))

        #Posting DTW data on the DB.
        slc_counter = 1
        return_objs = []
        dtw_serializer_class = ''
        for value in similar_vals:
            
            slc = value[0]
            diff = value[1]
            
            index_start = 0
            index_end = 0
            exists = False
            for i in range(len(results_atr) - 1):
                if slc == results_atr[i:i+(len(slc))]:
                    index_start = i
                    index_end = i+(len(slc)) 
                    break

            times = timed_data[index_start:index_end]
            
            segment_match_str = segment_str + 'Slice' + str(slc_counter)
            for t in times: 
                dtw_object = DtwMatch(field = attribute, value = t[0], segment_query = segment_str, segment_match = segment_match_str, difference = diff, threshold = threshold_arg, time = t[1])
                dtw_object.save()
            
            slc_counter +=1
            
            return_objs.append(dtw_object)
        
        #Returning the posted data.
        dtw_serializer = DtwMatchSerializer(return_objs, many=True)
        return Response(dtw_serializer.data)

    #Getting SimilarSpans data.
    def get(self, request, format=None):

        query_arg = ''
        if 'query' not in request.GET:
            return Response('Query argument is not set.')
        else:
            query_arg = request.GET['query']

        query_arg = ast.literal_eval(query_arg)

        table = ''
        if 'table' not in request.GET or request.GET['table'] not in iot_models:
            return Response('Table argument is not set.')
        else:
            table = request.GET['table']
        

        difference_arg = 0.0
        if 'difference' not in request.GET:
            difference_arg = 1.2
        else:
            difference_arg = request.GET['difference']
            
            is_float = False

            try:
                float(difference_arg)
                is_float = True
            except ValueError:
                is_float = False

            if is_float:
                difference_arg = float(difference_arg)
            else:
                difference_arg = 1.2

        
        attribute = attr_dict[table]
        results_all = DtwQuery.objects.all()
        results_all = results_all.filter(field=attribute).order_by('id_token')

        
        results_float = [float(getattr(r, 'value')) for r in results_all]
        #exists = any(query_arg == results_float[i:i+(len(query_arg))] for i in range(len(results_float) - 1))

        #print(query_arg)
        #print(results_float)

        #Verifying the requested query is already posted.
        index_start = 0
        index_end = 0
        exists = False
        segment_id = ''
        for i in range(len(results_float) - 1):
            if query_arg == results_float[i:i+(len(query_arg))]:
                exists = True
                index_start = i
                index_end = i+(len(query_arg)) 
                for index, item in enumerate(results_all):
                    if index == index_start:
                        segment_id = item.segment_query
                break


        if exists != True:
            return Response('Query is not in the table.')
        
        #Getting the matched slices.
        matches_all = DtwMatch.objects.all()
        matches_all = matches_all.filter(field=attribute,segment_query=segment_id, difference__lte=difference_arg)
        
        dtw_serializer = DtwMatchSerializer(matches_all, many=True)    
        
        return Response(dtw_serializer.data)

    
#Class used for the ADL classification-related stuff endpoint.
class AdlClassify(APIView):

    permission_classes = [AllowAny]

    #Posting the signal of a volunteer to classify.
    def post(self, request, format=None):

        parameters = request.data.keys()

        vol = ''
        if 'volunteer' not in parameters:
            return Response('You must set a volunteer.')
        else:
            vol = request.data['volunteer']

        device1 = 'iot1'+vol
        command1 = 'kubectl run ' + device1 + ' --image=registry.gitlab.com/dataeng/internships/tsmgmt/iot-device-1:v5 --overrides=\'{ "apiVersion": "v1", "spec": {"imagePullSecrets": [{"name": "regcred"}]} }\' --command -- python3 /opt/vb/iot1.py classify ' + vol + ' '
        os.system(command1)

        return Response('All required devices have started running successfully.')

    #Getting data from the classification-related tables.
    def get(self, request, format=None):
        
        table = ''
        if 'table' not in request.GET or request.GET['table'] not in classify_tables:
            return Response('Table argument is not set.')
        else:
            table = request.GET['table']

        model = apps.get_model('demoapp',classify_tables[table])
        models = model.objects.all()
        
        if 'volunteer' in request.GET:
            vol_arg = request.GET['volunteer']
            models = models.filter(volunteer=vol_arg)

        if 'segment_query' in request.GET:
            argument = request.GET['segment_query']
            if table == 'adl_classify_1':
                models = models.filter(segment=argument)
            else:
                models = models.filter(segment_query=argument)


        if table != 'adl_classify_1':
            
            if 'volunteer_match' in request.GET:
                vol_arg = request.GET['volunteer_match']
                models = models.filter(volunteer_match=vol_arg)

            if 'segment_match' in request.GET:
                argument = request.GET['segment_match']
                models = models.filter(segment_match=argument)
            
            if 'label_match' in request.GET:
                argument = request.GET['label_match']
                models = models.filter(label_match=argument)
            
            if 'label_true' in request.GET:
                argument = request.GET['label_true']
                models = models.filter(label_match=argument)

            if 'difference' in request.GET:
                argument = request.GET['difference']
                models = models.filter(difference__lte=argument)

        
        dtw_serializer_class = serializers_dict[model.__name__]
        dtw_serializer = dtw_serializer_class(models, many=True)


        return Response(dtw_serializer.data)

    #Deleting the pods used for ADL-classify creation.
    def delete(self, request, format=None):

        parameters = request.data.keys()

        device = ''
        if 'device_name' not in parameters:
            return Response('You must set the device name.')
        else:
            device = request.data['device_name']

        
        command1 = 'kubectl delete pod ' + device + ' '
        #os.system(command1)
        try:
            subprocess.check_output([command1],  shell=True, stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError as e:
            return Response('The pod you provided does not exist.')

        return Response('The required device has been successfully deleted.')

#This class is used to populated the DB so the user may easily start the classification of a volunteer.
class PopulateDB(APIView):

    permission_classes = [AllowAny]

    def post(self, request, format=None):

        parameters = request.data.keys()
        
        vol = ''
        if 'volunteer' not in parameters:
            return Response('You must set a volunteer.')
        else:
            vol = request.data['volunteer']

        range_1 = range(1, 12)
        list_1 = list(range_1)
        str_list1 = []
        for i in list_1:
            str_list1.append('m'+str(i))

        range_2 = range(1, 6)
        list_2 = list(range_2)
        str_list2 = []
        for i in list_2:
            str_list2.append('f'+str(i))

        vols = str_list1+str_list2

        for v in vols:
            if v == vol:
                continue

            device2 = 'iot2'+v 
            command2 = 'kubectl run ' + device2 + ' --image=registry.gitlab.com/dataeng/internships/tsmgmt/iot-device-2:v4 --overrides=\'{ "apiVersion": "v1", "spec": {"imagePullSecrets": [{"name": "regcred"}]} }\' --command -- python3 /opt/vb/iot2.py adl ' + v + ' '
            os.system(command2)

            device1 = 'iot1'+v 
            command1 = 'kubectl run ' + device1 + ' --image=registry.gitlab.com/dataeng/internships/tsmgmt/iot-device-1:v5 --overrides=\'{ "apiVersion": "v1", "spec": {"imagePullSecrets": [{"name": "regcred"}]} }\' --command -- python3 /opt/vb/iot1.py adl ' + v + ' '
            os.system(command1)

        return Response('All required devices have started running successfully.')

#Deleting the pods used to populated the DB.
class DepopulateDB(APIView):

    permission_classes = [AllowAny]

    def delete(self, request, format=None):

        parameters = request.data.keys()
        
        vol = ''
        if 'volunteer' not in parameters:
            return Response('You must set a volunteer.')
        else:
            vol = request.data['volunteer']

        range_1 = range(1, 12)
        list_1 = list(range_1)
        str_list1 = []
        for i in list_1:
            str_list1.append('m'+str(i))

        range_2 = range(1, 6)
        list_2 = list(range_2)
        str_list2 = []
        for i in list_2:
            str_list2.append('f'+str(i))

        vols = str_list1+str_list2

        for v in vols:
            if v == vol:
                continue

            device2 = 'iot2'+v 
            command2 = 'kubectl delete pod ' + device2 + ' '
            os.system(command2)

            device1 = 'iot1'+v 
            command1 = 'kubectl delete pod ' + device1 + ' '
            os.system(command1)

        return Response('All required devices have been deleted successfully.')

#Deleting a non-specific pod.
class DeleteDevice(APIView):

    permission_classes = [AllowAny]

    def delete(self, request, format=None):

        parameters = request.data.keys()

        device = ''
        if 'device_name' not in parameters:
            return Response('Device name is not set.')
        else:
            device = request.data['device_name']

        command = 'kubectl delete pod ' + device +  ' '
        try:
            subprocess.check_output([command],  shell=True, stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError as e:
            return Response('The pod you provided does not exist.')

        return Response('The required device has been successfully deleted.')    


