from django.urls import path, include
from .views import *
from rest_framework import routers

from demoapp.views import *

#Defining the URLs of the django server, using our views. 

router = routers.DefaultRouter()
#router.register(r'analytics', AnalyticsViewSet)

RandomDataView = RandomData.as_view()
AdlDataView = AdlData.as_view()
SimilarSpansView = SimilarSpans.as_view()
AdlClassifyView = AdlClassify.as_view()
PopulateDBView = PopulateDB.as_view()
DepopulateDBView = DepopulateDB.as_view()
DeleteDeviceView = DeleteDevice.as_view()

#Defining URL patterns.
urlpatterns = [

    path(r'', include(router.urls)),
    path('RandomData', RandomDataView, name='random-data'),
    path('AdlData', AdlDataView, name='adl-data'),
    path('SimilarSpans', SimilarSpansView, name='similar-spans'),
    path('AdlClassify', AdlClassifyView, name='adl-classify'),
    path('PopulateDB', PopulateDBView, name='populate-db'),
    path('DepopulateDB',DepopulateDBView, name='depopulate-db'),
    path('DeleteDevice', DeleteDeviceView, name='delete-device')
] 
