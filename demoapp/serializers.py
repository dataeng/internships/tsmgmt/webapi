from rest_framework import serializers
from .models import *


#Defining Serializer classes for our Django models. A serializer is used to return data from Django in JSON format.
class AnalyticsSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Analytics01 #Model to serialize

        #Fields to be returned 
        fields = ['pk', 'calculation', 'temperature', 'ph', 'time']
        


class PhSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Ph01 

        fields = ['pk', 'ph', 'time']
        

class TemperatureSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Temp01

        fields = ['pk', 'temperature', 'time']


class DtwQuerySerializer(serializers.ModelSerializer):
    
    class Meta:
        model = DtwQuery 

        fields = ['pk', 'field','value','segment_query', 'time']

class DtwMatchSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = DtwMatch

        fields = ['pk', 'field','value','segment_query', 'segment_match', 'difference', 'threshold', 'time']
        
class AdlAccSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = AdlAcc01

        fields = ['pk', 'x', 'y', 'z', 'volunteer', 'time']

class AdlLabelsSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = AdlLabels01

        fields = ['pk', 'x', 'y', 'z', 'volunteer', 'activity', 'time']

class AdlClassifySerializer(serializers.ModelSerializer):
    
    class Meta:
        model = AdlClassify1

        fields = ['pk', 'x', 'y', 'z', 'volunteer', 'segment', 'time']

class AdlClassifyMatchesSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = AdlClassifyMatches2

        fields = ['pk', 'x', 'y', 'z', 'volunteer', 'volunteer_match', 'segment_query', 'segment_match', 'label_true', 'label_match', 'difference', 'time']


class AdlClassifyLabelsSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = AdlClassifyLabels2

        fields = ['pk', 'volunteer', 'volunteer_match', 'segment_query', 'segment_match', 'label_true', 'label_match', 'difference']

class FinishingTimesSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = FinishingTimes01

        fields = ['pk', 'x', 'y', 'z', 'activity', 'volunteer', 'time']

class SequenceSerializer(serializers.ModelSerializer):

    class Meta:
        model = SequenceModel

        fields = ['pk', 'field', 'query']

#Important note: This dictionary needs to be updated whenever a new model/serializer is added.

serializers_dict = {"Analytics01":AnalyticsSerializer, "Ph01":PhSerializer, "Temp01":TemperatureSerializer, "DtwQuery":DtwQuerySerializer , "DtwMatch":DtwMatchSerializer,  "AdlClassify1":AdlClassifySerializer, "AdlClassifyMatches2":AdlClassifyMatchesSerializer, "AdlClassifyLabels2": AdlClassifyLabelsSerializer, "FinishingTimes01":FinishingTimesSerializer, "SequenceModel":SequenceSerializer}

