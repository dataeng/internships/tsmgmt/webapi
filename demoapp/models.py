from django.db import models

# Create your models here.

#The models are the tables of our time series DB.
class Analytics01(models.Model):
    calculation = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    temperature = models.DecimalField(max_digits=4, decimal_places=2, blank=True, null=True)
    ph = models.DecimalField(max_digits=4, decimal_places=2, blank=True, null=True)
    time = models.DateTimeField(primary_key = True)

    class Meta:
        db_table = 'analytics_01'


class Ph01(models.Model):
    ph = models.DecimalField(max_digits=4, decimal_places=2, blank=True, null=True)
    time = models.DateTimeField(primary_key = True)

    class Meta:
        db_table = 'ph_01'
        
        
class Temp01(models.Model):
    temperature = models.DecimalField(max_digits=4, decimal_places=2, blank=True, null=True)
    time = models.DateTimeField(primary_key = True)

    class Meta:
        db_table = 'temp_01'
        

class AdlAcc01(models.Model):
    x = models.IntegerField(blank=True, null=True)
    y = models.IntegerField(blank=True, null=True)
    z = models.IntegerField(blank=True, null=True)
    volunteer = models.CharField(max_length=50, blank=True, null=True)
    time = models.DateTimeField(primary_key = True)

    class Meta:
        db_table = 'adl_acc_01'

class AdlLabels01(models.Model):
    x = models.IntegerField(blank=True, null=True)
    y = models.IntegerField(blank=True, null=True)
    z = models.IntegerField(blank=True, null=True)
    activity = models.CharField(max_length=50, blank=True, null=True)
    volunteer = models.CharField(max_length=50, blank=True, null=True)
    time = models.DateTimeField(primary_key = True)

    class Meta:
        db_table = 'adl_labels_01'


class AdlClassify1(models.Model):
    x = models.IntegerField(blank=True, null=True)
    y = models.IntegerField(blank=True, null=True)
    z = models.IntegerField(blank=True, null=True)
    volunteer = models.CharField(max_length=50, blank=True, null=True)
    segment = models.CharField(max_length=50, blank=True, null=True)
    time = models.DateTimeField(primary_key = True)

    class Meta:
        db_table = 'adl_classify_1'


class AdlClassifyMatches2(models.Model):
    x = models.IntegerField(blank=True, null=True)
    y = models.IntegerField(blank=True, null=True)
    z = models.IntegerField(blank=True, null=True)
    volunteer = models.CharField(max_length=50, blank=True, null=True)
    volunteer_match = models.CharField(max_length=50, blank=True, null=True)
    segment_query = models.CharField(max_length=50, blank=True, null=True)
    segment_match = models.CharField(max_length=50, blank=True, null=True)
    label_true = models.CharField(max_length=50, blank=True, null=True)
    label_match = models.CharField(max_length=50, blank=True, null=True)
    difference = models.DecimalField(max_digits=4, decimal_places=2, blank=True, null=True)
    time = models.DateTimeField(primary_key = True)

    class Meta:
        db_table = 'adl_classify_matches_2'

class AdlClassifyLabels2(models.Model):
    volunteer = models.CharField(max_length=50, blank=True, null=True)
    volunteer_match = models.CharField(max_length=50, blank=True, null=True)
    segment_query = models.CharField(max_length=50, primary_key = True)
    segment_match = models.CharField(max_length=50, blank=True, null=True)
    label_true = models.CharField(max_length=50, blank=True, null=True)
    label_match = models.CharField(max_length=50, blank=True, null=True)
    difference = models.DecimalField(max_digits=4, decimal_places=2, blank=True, null=True)

    class Meta:
        db_table = 'adl_classify_labels_2'

 
class DtwMatch(models.Model):
    field = models.CharField(max_length=50, blank=True, null=True)
    value = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    segment_query = models.CharField(max_length=50, blank=True, null=True)
    segment_match = models.CharField(max_length=50, blank=True, null=True)
    difference = models.DecimalField(max_digits=4, decimal_places=2, blank=True, null=True)
    threshold = models.DecimalField(max_digits=4, decimal_places=2, blank=True, null=True)
    time = models.DateTimeField()
    id_token = id_token = models.AutoField(primary_key = True)


    class Meta:
        db_table = 'dtw_match'


class DtwQuery(models.Model):
    field = models.CharField(max_length=50, blank=True, null=True)
    value = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    segment_query = models.CharField(max_length=50, blank=True, null=True)
    time = models.DateTimeField()
    id_token = id_token = models.AutoField(primary_key = True)

    class Meta:
        db_table = 'dtw_query'


class FinishingTimes01(models.Model):
    x = models.IntegerField(blank=True, null=True)
    y = models.IntegerField(blank=True, null=True)
    z = models.IntegerField(blank=True, null=True)
    activity = models.CharField(max_length=50, blank=True, null=True)
    volunteer = models.CharField(max_length=50, blank=True, null=True)
    time = models.DateTimeField(primary_key = True)

    class Meta:
        db_table = 'finishing_times_01'


#This one is an exception. It is used only by Django in RandomData GET to present the data in a sequence mode, so the user can easily pick the values he needs.
class SequenceModel(models.Model):

    field = models.CharField(max_length=50, primary_key = True)
    query = models.TextField(blank=True, null=True)

    class Meta:
        managed = False



#Important note: These dictionaries need to be updated whenever a new model is added on the DB.
iot_models = {"analytics_01":"Analytics01", "ph_01":"Ph01", "temp_01":"Temp01"}
attr_dict = {"analytics_01":"calculation", "ph_01":"ph", "temp_01":"temperature"}

classify_tables = {"adl_classify_1":"AdlClassify1","adl_classify_matches_2":"AdlClassifyMatches2","adl_classify_labels_2":"AdlClassifyLabels2" }
activity_table = ['brush_teeth', 'climb_stairs', 'comb_hair', 'descend_stairs', 'drink_glass', 'eat_meat', 'eat_soup', 'getup_bed', 'liedown_bed', 'pour_water', 'sitdown_chair', 'standup_chair', 'use_telephone', 'walk' ]
